# Classificateur

Projet Classificateur

Adam Meddahi - Groupe 4 <br />
Florian Queudot - Groupe 3

# L'algorithme :
L'algorithme de classification utilisé est l'algorithme PageRank vu en cours, auquel ont été 
apportées quelques modifications. Voici un rappel de son fonctionnement :

Le principe est de calculer un vecteur p_n qui contient les probabilités que chaque élève
soit pertinent.
Plus n est grand, plus p_n devrait etre précis.

p_0 est le vecteur (1/N, 1/N, ..., 1/N), avec N le nombre d'élèves.
pour obtenir le vecteur p_(n+1), on a p_(n+1) =  p_n * M,
où M est la matrice de coefficients a_ij, avec i et j sont des logins d'élèves. 

Calcul de a_ij:
* Si i ne vote pour personne, alors a_ij = 1 / N ; avec N le nombre d'élèves,
* Sinon, a_ij = (nb de votes de i vers j) / (nb de votes total de i).


La valeur de l'ordre n a été fixée à 250.


La formule de Brin et Pages est uilisée pour diluer la matrice des poids de votes, avec une 
valeur d'alpha fixée à 0.15.


## Organisation du code :

Langages utilisés : HTML et JS

Les fichiers :

***index.html :*** le point d'entrée de l'application.

***main.js :*** le fichier js principal qui est chargé par index.html.

***pagerank_worker.js :*** le code lancé par main.js qui s'occupe de calculer le classement des 
élèves selon les votes et l'importance de chaque matière. Il est lancé dans un nouveau thread
afin de ne pas bloquer l'interface graphique lors des calculs. La technologie utilisée pour ce 
faire ne marche que si le JS tourne sur un serveur web, et non en local.

***style.css :*** feuile de style pour les éléments HTML générés. 

Attention, pagerank_worker.js ne marche que s'il tourne sur un serveur web, et non en local.

Le classificateur est consultable à l'url : 
http://dwarves.iut-fbleau.fr/~queudot/classificateur/
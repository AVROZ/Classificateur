console.log("votes :");console.log(votes);
console.log("logins :");console.log(logins);

// Un tableau contenant le nom des matières 
let subjects = getSubjects(votes); 

// l'ordre n auquel calculer par récursivité le vecteur de scores 
let degree = 250; 

 // la valeur alpha dans la formule de Brin et Pages
let alpha = 0.15;

// ajoute les sliders qui permettent à l'utilisateur de changer l'importance de chaque matière
addSliders(subjects); 

// Quand le bouton est cliqué, les résultats sont calculés à partir des votes et de l'importance
// des matières. 
document.getElementById('results-btn').onclick = () => refreshResults(votes, logins, subjects, degree, alpha);//displayAll(rank(votes, logins, subjects, 100));


// Calcule et affiche le classement.
refreshResults(votes, logins, subjects, degree, alpha);


// Renvoie un tableau contenant les matières à utiliser pour la classification d'après 
// les noms de propriétés correspondant aux matières dans la variable votes récupérée.  
function getSubjects(votes) {
	let subjects = [];

	for (const login in votes) {
		if (votes.hasOwnProperty(login)) {
			const votesRow = votes[login];
			for (const subject in votesRow) {
				if (votesRow.hasOwnProperty(subject)) {
					subjects.push(subject);
				}
			}

			break;
		}
	}

	return subjects;
}

// Ajoute au HTML des sliders qui permettent à l'utilisateur de choisir l'importance de 
// chaque matière contenue dans le tableau donné en argument.
function addSliders(subjects) {
	let slidersContainer = document.getElementById('sliders');
	slidersContainer.innerHTML = '';

	subjects.forEach(subject => addSlider(slidersContainer, subject));
}

// Ajoute dans l'élément HTML donné un slider qui permet à l'utilisateur de choisir 
// l'importance de la matière donnée.
// Le slider prend les valeurs entières de 0 à 10;
function addSlider(container, subject) {
	let rowContainer = createElement('div', 'slider-container'); 

	let label = createElement('label', 'slider-label', subject + ": ");
	label.for = subject;
	rowContainer.appendChild(label);

	let input = createElement('input', 'slider focusable');
	input.type = 'checkbox';
	input.id = subject + '-slider';
	input.name = subject;
	// input.min = 0;
	// input.max = 10;
	input.value = 0;
	// input.step = 1;
	input.oninput = () =>  updateRangeText(subject);
	rowContainer.appendChild(input);

	let output = createElement('span', 'slider-output', getCheckboxValue(input));
	output.id = subject + '-slider-output';
	rowContainer.appendChild(output);

	container.appendChild(rowContainer);
}

// Met à jour le texte dans l'élément qui permet de voir la valeur du slider qui 
// correspond à la matière donnée.
//
// N.B. cet élément est invisible sur la page par défaut.
function updateRangeText(subject) {
	let input = document.getElementById(subject + '-slider');
	let output = document.getElementById(subject + '-slider-output');
	output.innerHTML = getCheckboxValue(input);
}

// Renvoie un objet de la forme { matiere_1: X_1, matiere_2: X_2, ... }
// où matiere_i est le nom d'une matière dans le tableau donné en argument
// et X_i est le coefficient des votes dans cette matière. 
// 
// Si le slider pour matiere_i a une valeur de x_i, alors X_i = (2^x_i) - 1.
function getSubjectWeights(subjects) {
	let weights = {};

	subjects.forEach( subject => {
		let value = document.getElementById(subject + '-slider').value;

		weights[subject] = (1 << parseInt(value, 10)) - 1;
	});

	return weights;
}

// La fonction appelée quand les scores de chaque élève doivent être calculés et affichés.
// Un nouveau thread est créé pour les calculs. Quand les calculs sont terminés, le thread 
// renvoie les scores triés, qui sont ajoutés dans le HTML.
function refreshResults(votes, logins, subjects, degree, alpha = 0.0) {
	let container = document.getElementById('results');
	if(container == undefined)
		alert('No element with id "results".');
	container.innerHTML = 'Calcul en cours...';

	if (window.Worker) {
		let prWorker = new Worker('pagerank_worker.js');
		prWorker.onmessage = function(e) {
			console.log('Main received msg from prWorker :'); console.log(e.data);
			console.log('e.origin : '); console.log(e.origin);

			// Afficher les résultats reçus du thread créé.
			showAll(e.data, container);

			console.log('Terminating prWorker.');
			prWorker.terminate();
		}

		// Envoyer les données nécessaires pour commencer la classification
		prWorker.postMessage({
			votes: votes,
			logins: logins,
			subjectWeights: getSubjectWeights(subjects),
			degree: degree,
			alpha: alpha
		});
	}
}

// Affiche le classement donné dans l'élément donné. 
// Si deux élèves ont le même score, leur rang est égal et ils sont affichés dans l'ordre
// du tableau (qui devrait être alphabétique).
function showAll(ranks, container) {
	container.innerHTML = '';

	let rankProbability = 2.0;
	let rank = 0;
	for (let i = 0; i < ranks.length; i++) {
		const person = ranks[i];
		if (person.probability < rankProbability) {
			rank++;
			rankProbability = person.probability;
		}
		show(rank, person, container);
	}
}

// Ajoute dans l'élément donné une nouvelle ligne qui a le rang donné et dont le nom, login 
// et score sont dans listItem.
function show( rank, listItem, container ) {
	let div = createElement('div', 'list-item');
	
	div.appendChild(createElement('span', 'rank', rank));
	div.appendChild(createElement('span', 'name', listItem.name));
	div.appendChild(createElement('span', 'login', listItem.login));
	div.appendChild(createElement('span', 'probability', listItem.probability));

	container.appendChild(div);
}

// Crée un élément HTML.
function createElement(type, className = '', innerHTML = '') {
	let element = document.createElement(type);
	element.className = className;
	element.innerHTML = innerHTML;
	return element;
}

function getCheckboxValue(input) {
	if (input.checked == true) {
		input.value = 1;
	} else {
		input.value = 0;
	}
}